﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowController : MonoBehaviour
{
    public GameObject BallPrefab;
    float spen = 30.0f;
    float delta = 0;
    float a = 0.5f;
    float b = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.A))
        {
            transform.Rotate(0, 0, -5);
        }

        if(Input.GetKey(KeyCode.D))
        {
            transform.Rotate(0, 0, 5);
        }

        this.delta += Time.deltaTime;
        this.b += Time.deltaTime;
        if(this.spen > this.delta)
        {
            if(Input.GetKeyDown(KeyCode.W))
            {
                if(this.b > this.a)
                {
                    this.b = 0;
                    GameObject ball = Instantiate(BallPrefab) as GameObject;
                    ball.transform.position = transform.position;
                    ball.transform.rotation = transform.rotation;
                }
            }
        }
    }
}
