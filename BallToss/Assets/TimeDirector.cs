﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeDirector : MonoBehaviour
{
    GameObject timerOb;
   
    
    // Start is called before the first frame update
    void Start()
    {
        this.timerOb = GameObject.Find("Timer");
    }

    // Update is called once per frame
    void Update()
    {
        this.timerOb.GetComponent<Image>().fillAmount -= 0.00056f;
        
    }
}
