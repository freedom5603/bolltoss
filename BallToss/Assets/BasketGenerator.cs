﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasketGenerator : MonoBehaviour
{
    public GameObject BasketPrefab;
    float spen = 0.8f;
    float delta = 0;

    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
        if(this.delta > this.spen)
        {
            this.delta = 0;
            GameObject go = Instantiate(BasketPrefab) as GameObject;
            go.transform.position = new Vector3(-10, -3.7f, 0);
        }
    }
}
