﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasketController : MonoBehaviour
{
    GameObject player;
    float px = 0;

    // Start is called before the first frame update
    void Start()
    {
        px = Random.Range(0.1f, 0.3f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(px, 0, 0);

        if(transform.position.x > 12)
        {
            Destroy(gameObject);
        }
    }    
}
