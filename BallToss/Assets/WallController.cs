﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallController : MonoBehaviour
{
    GameObject wall;
    float spen = 1.0f;
    float delta = 0;
    // Start is called before the first frame update
    void Start()
    {
        this.wall = GameObject.Find("wallPrafab");
    }

    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
        if(this.delta > this.spen)
        {
            Destroy(gameObject);
        }
    }
}
