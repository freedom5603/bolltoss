﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManegiment;

public class CountDirector : MonoBehaviour
{
    public int Count = 0;
    public int Count2 = 0;
    public GameObject Counter = null;
    public GameObject Counter2 = null;
    float spen = 30.0f;
    float delta = 0;
    // Start is called before the first frame update
    void Start()
    {
        this.Counter = GameObject.Find("Counter");
        this.Counter2 = GameObject.Find("Counter2");
    }

    public void IncreaseCo()
    {
        Count++;
    }

    public void IncreaseCoo()
    {
        Count2++;
    }


    // Update is called once per frame
    void Update()
    {
        Text arrowText = this.Counter.GetComponent<Text>();
        arrowText.text = Count + "個";
        Text arrowText2 = this.Counter2.GetComponent<Text>();
        arrowText2.text = Count2 + "個";

        this.delta += Time.deltaTime;
        if(this.delta > this.spen)
        {

        }
    }
}
