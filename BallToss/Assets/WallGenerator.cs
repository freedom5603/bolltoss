﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallGenerator : MonoBehaviour
{
    public GameObject wallPrafab;
    float spen = 1.0f;
    float delta = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
        if(this.delta > this.spen)
        {
            this.delta = 0;
            GameObject wall = Instantiate(wallPrafab) as GameObject;
            int px = Random.Range(-4, 4);
            wall.transform.position = new Vector3(px, -0.5f, 0);
        }
    }
}
