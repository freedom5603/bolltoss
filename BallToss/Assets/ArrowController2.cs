﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowController2 : MonoBehaviour
{
    public GameObject BallPrefab2;
    float spen = 30.0f;
    float delta = 0;
    float a = 0.5f;
    float b = 0;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(0, 0, -5);
        }

        if(Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(0, 0, 5);
        }

        this.delta += Time.deltaTime;
        this.b += Time.deltaTime;
        if (this.spen > this.delta)
        {
            if(Input.GetKeyDown(KeyCode.UpArrow))
            {
                if(this.b > this.a)
                {
                    this.b = 0;
                    GameObject ball = Instantiate(BallPrefab2) as GameObject;
                    ball.transform.position = transform.position;
                    ball.transform.rotation = transform.rotation;
                }
            }
        }
    }
}
